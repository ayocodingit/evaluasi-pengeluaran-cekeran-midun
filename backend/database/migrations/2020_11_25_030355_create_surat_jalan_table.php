<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSuratJalanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surat_jalan', function (Blueprint $table) {
            $table->id();
            $table->string('nomor')->unique();
            $table->date('tanggal');
            $table->bigInteger('total_pengeluaran')->nullable();
            $table->bigInteger('total_penjualan')->nullable();
            $table->bigInteger('keuntungan')->nullable();
            $table->double('persentase')->nullable();
            $table->enum('status', ['pemesanan', 'terima-barang', 'tutup-buku'])->default('pemesanan');
            $table->foreignId('cabang_id')
                ->constrained('cabang')
                ->onDelete('set null')
                ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('surat_jalan', function (Blueprint $table) {
            $table->dropForeign('cabang_id');
        });
        Schema::dropIfExists('surat_jalan');
    }
}
