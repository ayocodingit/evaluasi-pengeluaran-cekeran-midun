<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePemesananTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pemesanan', function (Blueprint $table) {
            $table->id();
            $table->string('nama');
            $table->integer('harga');
            $table->integer('qty');
            $table->string('satuan')->nullable();
            $table->string('kategori')->nullable();
            $table->foreignId('surat_jalan_id')
                ->constrained('surat_jalan')
                ->onUpdate('cascade')
                ->onDelete('set null');
            $table->foreignId('bahan_baku_id')
                ->constrained('bahan_baku')
                ->onUpdate('no action')
                ->onDelete('set null');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pemesanan', function (Blueprint $table) {
            $table->dropForeign('bahan_baku_id');
            $table->dropForeign('surat_jalan_id');
        });
        Schema::dropIfExists('pemesanan');
    }
}
