<?php

use App\Models\Cabang;
use Illuminate\Database\Seeder;

class CabangSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->items() as $item) {
            Cabang::updateOrCreate(['id' => $item['id']], $item);
        }
    }

    public function items(): array
    {
        $csv = array_map('str_getcsv', file(base_path() . "/database/seeds/csv/cabang.csv"));
        $data = [];

        foreach ($csv as $key => $value) {
            if ($key == 0) continue;
            $item['id'] = $value[0];
            $item['nama'] = $value[1];
            $item['alamat'] = $value[2];
            $item['kode'] = $value[3];

            array_push($data, $item);
        }

        return $data;
    }
}
