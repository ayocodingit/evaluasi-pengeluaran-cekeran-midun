<?php

use App\Models\SuratJalan;
use Illuminate\Database\Seeder;

class SuratJalanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->items() as $item) {
            SuratJalan::updateOrCreate(['nomor' => $item['nomor']], $item);
        }
    }

    public function items(): array
    {
        $csv = array_map('str_getcsv', file(base_path() . "/database/seeds/csv/surat_jalan.csv"));
        $data = [];

        foreach ($csv as $key => $value) {
            if ($key == 0) continue;
            $item['nomor'] = $value[0];
            $item['tanggal'] = $value[1];
            $item['total_pengeluaran'] = (double) $value[2];
            $item['total_penjualan'] = (double) $value[3];
            $item['keuntungan'] = (double) $value[4];
            $item['status'] = $value[5];
            $item['cabang_id'] = $value[6];
            $item['persentase'] = (double) $value[7];

            array_push($data, $item);
        }

        return $data;
    }
}
