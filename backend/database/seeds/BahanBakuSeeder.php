<?php

use App\Models\BahanBaku;
use Illuminate\Database\Seeder;

class BahanBakuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->items() as $item) {
            BahanBaku::updateOrCreate(['nama' => strtoupper($item['nama'])], $item);
        }
    }

    public function items(): array
    {
        $csv = array_map('str_getcsv', file(base_path() . "/database/seeds/csv/bahan_baku.csv"));
        $data = [];

        foreach ($csv as $key => $value) {
            if ($key == 0) continue;
            $item['nama'] = $value[0];
            $item['harga'] = $value[1];
            $item['satuan'] = $value[2];
            $item['kategori'] = $value[3];

            array_push($data, $item);
        }

        return $data;
    }
}
