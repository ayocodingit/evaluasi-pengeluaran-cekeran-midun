<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CabangSeeder::class);
        $this->call(BahanBakuSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(SuratJalanSeeder::class);
    }
}
