<?php

use App\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->items() as $item) {
            User::updateOrCreate(['email' => $item['email']], $item);
        }
    }

    public function items(): array
    {
        $csv = array_map('str_getcsv', file(base_path() . "/database/seeds/csv/user.csv"));
        $data = [];

        foreach ($csv as $key => $value) {
            if ($key == 0) continue;
            $item['username'] = $value[0];
            $item['email'] = $value[1];
            $item['password'] = Hash::make($value[2]);
            $item['role'] = $value[3];
            $item['cabang_id'] = $value[4] ? (int)$value[4] : null;

            array_push($data, $item);
        }

        return $data;
    }
}
