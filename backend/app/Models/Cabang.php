<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cabang extends Model
{
    protected $table = 'cabang';

    protected $guarded = ['id', 'created_at', 'updated_at'];

    protected $hidden = [
        'created_at', 'updated_at',
    ];
}
