<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pemesanan extends Model
{
    protected $table = 'pemesanan';

    protected $guarded = ['id', 'created_at', 'updated_at'];

    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public function bahan_baku()
    {
        return $this->belongsTo(BahanBaku::class);
    }
}
