<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Penjualan extends Model
{
    protected $table = 'penjualan';

    protected $guarded = ['id', 'created_at', 'updated_at'];

    protected $hidden = [
        'created_at', 'updated_at',
    ];
}
