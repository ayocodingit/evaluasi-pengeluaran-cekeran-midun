<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class SuratJalan extends Model
{
    protected $table = 'surat_jalan';

    protected $guarded = ['id', 'created_at', 'updated_at'];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    protected $appends = [
        'tepat_waktu'
    ];

    public function pemesanan()
    {
        return $this->hasMany(Pemesanan::class);
    }

    public function penjualan()
    {
        return $this->hasOne(Penjualan::class);
    }

    public function cabang()
    {
        return $this->belongsTo(Cabang::class);
    }

    public function getTepatWaktuAttribute()
    {
        $jam = (int) Carbon::parse($this->created_at)->format('H');
        return $jam <= 10;
    }
}
