<?php

namespace App\Traits;

/**
 *
 */
trait ApiResponse
{
    protected function errorResponse($message, $code)
	{
		return response()->json([
			'code' => $code,
			'message' => $message
		], $code);
	}
}
