<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use App\Models\Penjualan;
use App\Models\SuratJalan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class PenjualanController extends Controller
{
    public function update(Request $request, $id)
    {
        Validator::make($request->toArray(), [
            'pendapatan_kotor' => 'required|integer',
            'pajak' => 'required',
            'pendapatan_bersih' => 'required|integer',
        ])->validate();
        DB::beginTransaction();
        try {
            $surat_jalan = SuratJalan::with('pemesanan')->findOrFail($id);
            if ($surat_jalan->penjualan) {
                $penjualan = Penjualan::findOrFail($surat_jalan->penjualan->id);
            } else {
                $penjualan = new Penjualan();
            }
            $penjualan->pendapatan_kotor = $request->pendapatan_kotor;
            $penjualan->pajak = (int) str_replace('%','',$request->pajak);
            $penjualan->pendapatan_bersih = $request->pendapatan_bersih;
            $penjualan->surat_jalan_id = $id;
            $penjualan->save();
            $total_penjualan = $penjualan->pendapatan_bersih;
            SuratJalan::findOrFail($id)->update([
                'total_penjualan' => $total_penjualan,
                'status' => 'tutup-buku',
                'keuntungan' => $total_penjualan - $surat_jalan->total_pengeluaran,
                'persentase' => ($total_penjualan - $surat_jalan->total_pengeluaran) * 100,
            ]);
            DB::commit();
            return response()->json(['code' => 200, 'message' => 'success'], 200);
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
            return response()->json(['code' => 500, 'message' => 'system error'], 200);
        }
    }
}
