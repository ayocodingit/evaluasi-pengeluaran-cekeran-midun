<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use App\Models\Cabang;
use App\Models\SuratJalan;
use Box\Spout\Writer\Style\StyleBuilder;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Rap2hpoutre\FastExcel\FastExcel;
use Rap2hpoutre\FastExcel\SheetCollection;

class LaporanController extends Controller
{
    public function laporanAll(Request $request)
    {
        $tanggal = $request->get('tanggal', false);
        $tanggal = explode('-', $tanggal);
        $bulan = $tanggal[1];
        $tahun = $tanggal[0];
        $data = $this->getDataAll($bulan, $tahun);
        return response()->json([
            'result' => $data,
            'code' => 200,
        ]);
    }

    public function laporanCabang(Request $request)
    {
        $cabang = $request->get('cabang', false);
        $tanggal = $request->get('tanggal', false);
        $tanggal = explode('-', $tanggal);
        $bulan = $tanggal[1];
        $tahun = $tanggal[0];
        $data = $this->getDataCabang($bulan, $tahun, $cabang);
        return response()->json([
            'result' => $data,
            'code' => 200,
        ]);
    }

    public function excel(Request $request)
    {
        $tanggal = $request->get('tanggal', false);
        $tanggal = explode('-', $tanggal);
        $bulan = $tanggal[1];
        $tahun = $tanggal[0];
        $namaFile = $this->generateExcel($bulan, $tahun);
        return $this->generateExcel($bulan, $tahun);
    }

    public function generateExcel($bulan, $tahun)
    {
        $sheets = [];
        $data = $this->getDataAll($bulan, $tahun);
        foreach ($data as $row) {
            $sheets['Seluruh Cabang'][] = [
                'Nama Cabang' => $row->cabang->nama,
                'Pengeluaran' => 'Rp. ' . number_format($row->total_pengeluaran, 0, ',', '.'),
                'Penjualan' => 'Rp. ' . number_format($row->total_penjualan, 0, ',', '.'),
                'Keuntungan' => 'Rp. ' . number_format($row->keuntungan, 0, ',', '.'),
            ];
        }
        if (count($data)) {
            $sheets['Seluruh Cabang'][] = [
                'Nama Cabang' => 'Total',
                'Pengeluaran' => 'Rp. ' . number_format($data->pluck('total_pengeluaran')->sum(), 0, ',', '.'),
                'Penjualan' => 'Rp. ' . number_format($data->pluck('total_penjualan')->sum(), 0, ',', '.'),
                'Keuntungan' => 'Rp. ' . number_format($data->pluck('keuntungan')->sum(), 0, ',', '.'),
            ];
        }
        $cabang = $this->cabang();
        foreach ($cabang as $cabang) {
            $data = $this->getDataCabang($bulan, $tahun, $cabang->id);
            foreach ($data as $row) {
                $sheets[$row->cabang->nama][] = [
                    'Nomor Surat Jalan' => $row->nomor,
                    'Tanggal' => Carbon::parse($row->tanggal)->translatedFormat('d F Y'),
                    'Pengeluaran' => 'Rp. ' . number_format($row->total_pengeluaran, 0, ',', '.'),
                    'Penjualan' => 'Rp. ' . number_format($row->total_penjualan, 0, ',', '.'),
                    'Keuntungan' => 'Rp. ' . number_format($row->keuntungan, 0, ',', '.'),
                ];
            }
            if (count($data)) {
                $sheets[$data[0]->cabang->nama][] = [
                    'Nomor Surat Jalan' => null,
                    'Tanggal' => 'Total',
                    'Pengeluaran' => 'Rp. ' . number_format($data->pluck('total_pengeluaran')->sum(), 0, ',', '.'),
                    'Penjualan' => 'Rp. ' . number_format($data->pluck('total_penjualan')->sum(), 0, ',', '.'),
                    'Keuntungan' => 'Rp. ' . number_format($data->pluck('keuntungan')->sum(), 0, ',', '.'),
                ];
            }
        }

        $header_style = (new StyleBuilder())->setFontBold()->build();
        $sheets = new SheetCollection($sheets);
        $bulan = Carbon::parse(date('Y-m-d', strtotime($tahun . '-' . $bulan . '-' . date('d'))))->translatedFormat('F');
        $namaFile = "Laporan-{$bulan}-{$tahun}.xlsx";
        return (new FastExcel($sheets))
            ->headerStyle($header_style)
            ->download($namaFile);
    }

    public function getDataCabang($bulan, $tahun, $cabang)
    {
        $data = SuratJalan::with('cabang')
            ->whereMonth('tanggal', $bulan)
            ->whereYear('tanggal', $tahun)
            ->where('cabang_id', $cabang)
            ->orderBy('tanggal', 'asc')
            ->get();
        return $data;
    }

    public function getDataAll($bulan, $tahun)
    {
        $data = SuratJalan::select('cabang_id', DB::raw("sum(total_pengeluaran)::bigInt as total_pengeluaran"),DB::raw("sum(total_penjualan)::bigInt as total_penjualan"),DB::raw("sum(keuntungan)::bigInt as keuntungan") )
            ->groupBy('cabang_id')
            ->with('cabang')
            ->whereMonth('tanggal', $bulan)
            ->whereYear('tanggal', $tahun)
            ->get();
        return $data;
    }

    public function cabang()
    {
        return Cabang::all();
    }
}
