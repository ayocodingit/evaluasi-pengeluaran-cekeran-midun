<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use App\Models\Cabang;
use App\Models\SuratJalan;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function cabang(Request $request)
    {
        $tanggal = $request->get('tanggal', date('Y-m'));
        $cabang_id = $request->get('cabang_id', false);
        $data['labels'] = [];
        $data['data'] = [];
        if ($cabang_id) {
            $days = Carbon::parse(date('Y-m-d', strtotime($tanggal . '-' . date('d'))))->daysInMonth;
            foreach (range(1, $days) as $day) {
                $date = date('Y-m-d', strtotime($tanggal . '-' . $day));
                $surat_jalan = $this->getDataCabang($cabang_id, $date);
                if (!$surat_jalan) {
                    continue;
                }
                $data['labels'][] = $date;
                $data['data']['total_pengeluaran'][] = optional($surat_jalan)->total_pengeluaran;
                $data['data']['total_penjualan'][] = optional($surat_jalan)->total_penjualan;
                $data['data']['persentase'][] = optional($surat_jalan)->keuntungan;

            }

            $cabang = Cabang::findOrFail($cabang_id)->nama;
            $data['text'] = "Keuntungan Cabang $cabang Periode";
            if (count($data['labels'])) {
                $data['subtitle'] = Carbon::parse(date('Y-m-d', strtotime($data['labels'][0])))->translatedFormat('d F Y') . ' - ' . Carbon::parse(date('Y-m-d', strtotime($data['labels'][count($data['labels']) - 1])))->translatedFormat('d F Y');
            } else {
                $data['subtitle'] = Carbon::parse($tanggal)->startOfMonth()->translatedFormat('d F Y') . ' - ' . Carbon::parse($tanggal)->endOfMonth()->translatedFormat('d F Y');
            }
            // $data['subtitle'] = Carbon::parse(date('Y-m-d', strtotime($tanggal . '-' . date('d'))))->startOfMonth()->translatedFormat('d F Y') . ' - ' . Carbon::parse(date('Y-m-d', strtotime($tanggal . '-' . date('d'))))->endOfMonth()->translatedFormat('d F Y');
        }

        return response()->json([
            'result' => $data,
            'code' => 200,
        ]);
    }

    public function perbandingan(Request $request)
    {
        $tanggal = $request->get('tanggal', date('Y-m'));
        $perbandingan = $this->getDataPerbandingan($tanggal);
        $data['tanggal'] = $tanggal . '-' . date('d');
        $data['labels'] = $perbandingan->pluck('nama');
        $data['data'] = $perbandingan->pluck('keuntungan');
        $data['total_pengeluaran'] = $perbandingan->pluck('total_pengeluaran');
        $data['total_penjualan'] = $perbandingan->pluck('total_penjualan');
        $data['text'] = "Tingkat Efisiensi Seluruh Cabang Periode";
        $data['subtitle'] = 'Bulan : ' . Carbon::parse($tanggal)->translatedFormat('F') . ' Tahun : ' . Carbon::parse(date('Y-m-d', strtotime($tanggal)))->translatedFormat('Y');
        return response()->json([
            'result' => $data,
            'code' => 200,
        ]);
    }

    public function getDataCabang($cabang_id, $tanggal)
    {
        return SuratJalan::with('cabang')
            ->where('cabang_id', $cabang_id)
            ->whereDate('tanggal', $tanggal)
            ->where('status', 'tutup-buku')
            ->first();
    }

    public function getDataPerbandingan($tanggal)
    {
        $bulan = explode('-', $tanggal)[1];
        $tahun = explode('-', $tanggal)[0];
        return SuratJalan::select(DB::raw('((sum(total_pengeluaran)/sum(total_penjualan))*100)::bigInt as keuntungan'), 'cabang.nama', DB::raw('sum(total_pengeluaran) as total_pengeluaran'), DB::raw('sum(total_penjualan) as total_penjualan'))
            ->leftJoin('cabang', 'cabang.id', 'surat_jalan.cabang_id')
            ->whereMonth('tanggal', $bulan)
            ->whereYear('tanggal', $tahun)
            ->where('status', 'tutup-buku')
            ->orderBy('keuntungan', 'desc')
            ->groupBy('cabang_id', 'cabang.nama')
            ->get();
    }
}
