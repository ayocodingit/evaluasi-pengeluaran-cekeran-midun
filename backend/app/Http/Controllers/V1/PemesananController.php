<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use App\Models\BahanBaku;
use App\Models\Pemesanan;
use App\Models\SuratJalan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class PemesananController extends Controller
{
    public function store(Request $request)
    {
        $error = 0;
        $data = [];
        $bahan_baku_id = [];

        foreach ($request->data as $item) {
            $validator = Validator::make($item, [
                'bahan_baku' => 'required|array',
                'qty' => 'required|integer|min:1',
            ]);
            if ($validator->fails()) {
                $item['error'] = $validator->errors()->first();
                $error++;
            } elseif (in_array($item['bahan_baku']['id'], $bahan_baku_id)) {
                $item['error'] = 'bahan_baku sudah ada sebelumnya';
                $error++;
            }
            $data[] = $item;
            $bahan_baku_id[] = $item['bahan_baku'] ? $item['bahan_baku']['id'] : null;
        }
        if (!$error) {
            DB::beginTransaction();
            try {
                $surat_jalan = new SuratJalan;
                $surat_jalan->nomor = $request->get('nomor');
                $surat_jalan->tanggal = date('Y-m-d');
                $surat_jalan->cabang_id = $request->user()->id;
                $surat_jalan->keterangan = $request->get('keterangan');
                $surat_jalan->save();
                foreach ($request->data as $item) {
                    $bahan_baku = BahanBaku::find($item['bahan_baku']['id']);
                    $pemesanan = new Pemesanan;
                    $pemesanan->bahan_baku_id = $bahan_baku->id;
                    $pemesanan->nama = $bahan_baku->nama;
                    $pemesanan->harga = $bahan_baku->harga;
                    $pemesanan->kategori = $bahan_baku->kategori;
                    $pemesanan->qty = $item['qty'];
                    $pemesanan->surat_jalan_id = $surat_jalan->id;
                    $pemesanan->save();
                }
                DB::commit();
                return response()->json(['code' => 200, 'message' => 'success'], 200);
            } catch (\Throwable $th) {
                DB::rollback();
                return response()->json(['code' => 500, 'message' => $th->getMessage()], 500);
            }
        } else {
            return response()->json(['code' => 422, 'message' => 'error', 'result' => $data], 200);
        }
    }

    public function template()
    {
        $models = BahanBaku::whereIn('nama', ['Ceker', 'Mie', 'Bihun','Sosis Ori'])->get();
        $data = [];
        foreach ($models as $value) {
            $data[] = [
                'bahan_baku' => $value,
                'qty' => 0,
                'error' => null
            ];
        }
        return response()->json(['code' => 200, 'result' => $data], 200);
    }

    public function show($id)
    {
        $models = SuratJalan::with(['cabang', 'pemesanan.bahan_baku'])->findOrFail($id);
        return response()->json(['code' => 200, 'result' => $models], 200);
    }

    public function delete($id)
    {
        try {
            $models = Pemesanan::findOrFail($id);
            $models->delete();
            return response()->json(['code' => 200, 'message' => 'success'], 200);
        } catch (\Throwable $th) {
            return response()->json(['code' => 500, 'message' => 'error'], 500);
        }
    }

    public function update(Request $request, $id)
    {
        $error = 0;
        $data = [];
        foreach ($request->data as $item) {
            $max = Pemesanan::findOrFail($item['id'])->qty;
            $validator = Validator::make($item, [
                'qty' => 'required|integer|min:1|max:' . $max,
            ]);

            if ($validator->fails()) {
                $item['error'] = $validator->errors()->first();
                $error++;
            }
            $data[] = $item;
        }
        if (!$error) {
            DB::beginTransaction();
            try {
                $total_pengeluaran = 0;
                foreach ($request->data as $item) {
                    $pemesanan = Pemesanan::findOrFail($item['id']);
                    $pemesanan->qty = $item['qty'];
                    $pemesanan->save();
                    $total_pengeluaran += $pemesanan->qty * $pemesanan->harga;
                }
                SuratJalan::findOrFail($id)->update([
                    'status' => 'terima-barang',
                    'total_pengeluaran' => $total_pengeluaran,
                ]);

                DB::commit();
                return response()->json(['code' => 200, 'message' => 'success'], 200);
            } catch (\Throwable $th) {
                DB::rollback();
                return response()->json(['code' => 500, 'message' => 'system error'], 500);
            }
        } else {
            return response()->json(['code' => 422, 'message' => 'error', 'result' => $data], 200);
        }
    }
}
