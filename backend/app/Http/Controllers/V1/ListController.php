<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use App\Models\BahanBaku;
use App\Models\Cabang;
use App\Models\SuratJalan;
use Illuminate\Http\Request;

class ListController extends Controller
{
    public function bahan_baku()
    {
        return response()->json(['code' => 200, 'message' => 'success', 'result' => BahanBaku::all()], 200);
    }

    public function cabang()
    {
        return response()->json(['code' => 200, 'message' => 'success', 'result' => Cabang::all()], 200);
    }

    public function surat_jalan(Request $request)
    {
        $user = $request->user();
        $models = SuratJalan::with('cabang');
        if ($user->role == 'cabang') {
            $models = $models->where('cabang_id', $user->cabang_id)
                ->with(['pemesanan', 'penjualan'])
                ->where('tanggal', date('Y-m-d'))
                ->first();
        } elseif ($user->role == 'ceo') {
            $models = $models->where('status', 'tutup-buku')->get();
        } else {
            $models = $models->where('tanggal', date('Y-m-d'))
                ->get();
        }
        return response()->json(['code' => 200, 'result' => $models], 200);
    }
}
