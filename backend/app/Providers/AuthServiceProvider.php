<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('isCabang', function($user) {
            return $user->role == 'cabang';
         });

        Gate::define('isProduksi', function($user) {
            return $user->role == 'produksi';
         });

        Gate::define('isCeo', function($user) {
            return $user->role == 'ceo';
         });
    }
}
