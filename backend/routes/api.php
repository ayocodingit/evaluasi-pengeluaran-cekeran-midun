<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'api'], function () {
    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
});

Route::group(['prefix' => 'v1', 'namespace' => 'V1'], function () {
    Route::group(['prefix' => 'pemesanan'], function () {
        Route::post('store', 'PemesananController@store')->middleware('can:isCabang');
        Route::get('template', 'PemesananController@template')->middleware('can:isCabang');
        Route::put('update/{id}', 'PemesananController@update')->middleware('can:isProduksi');
        Route::get('show/{id}', 'PemesananController@show')->middleware('can:isProduksi');
        Route::delete('delete/{id}', 'PemesananController@delete')->middleware('can:isProduksi');
    });

    Route::group(['prefix' => 'penjualan'], function () {
        Route::put('update/{id}', 'PenjualanController@update')->middleware('can:isCabang');
    });
    Route::group(['prefix' => 'dashboard', 'middleware' => 'can:isCeo'], function () {
        Route::get('cabang', 'DashboardController@cabang');
        Route::get('perbandingan', 'DashboardController@perbandingan');
    });
    Route::group(['prefix' => 'laporan'], function () {
        Route::get('laporan-all', 'LaporanController@laporanAll');
        Route::get('laporan-cabang', 'LaporanController@laporanCabang');
        Route::get('excel', 'LaporanController@excel');
    });
    Route::group(['prefix' => 'list'], function () {
        Route::get('surat-jalan', 'ListController@surat_jalan');
        Route::get('bahan-baku', 'ListController@bahan_baku');
        Route::get('cabang', 'ListController@cabang');
    });
});
