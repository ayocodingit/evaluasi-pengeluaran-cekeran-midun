import Vue from 'vue'
import VueI18n from 'vue-i18n'

Vue.use(VueI18n)

const i18n = new VueI18n({
  locale: 'id',
  fallbackLocale: 'id',
  messages: {}
})
loadMessages('id')
export default i18n

/**
 * @param {String} locale
 */
async function loadMessages(locale) {
  if (Object.keys(i18n.getLocaleMessage(locale)).length === 0) {
    const messages = await import(`../lang/${locale}`)
    i18n.setLocaleMessage(locale, messages)
  }
  if (i18n.locale !== locale) {
    i18n.locale = locale
  }
}