import Vue from 'vue'
import VueRouter from 'vue-router'
import Store from '../store'
import Home from '../views/Home.vue'
import Grafik from '../views/Ceo/Grafik.vue'
import Laporan from '../views/Ceo/Laporan.vue'
import Pemesanan from '../views/Pemesanan.vue'
import Penjualan from '../views/Penjualan.vue'
import Login from '../views/Login.vue'


Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    beforeEnter: (to, from, next) => {
      if (!Store.getters['auth/authenticated']) {
        return next({
          path: '/login'
        })
      }
      next()
    }
  },
  {
    path: '/grafik',
    name: 'Grafik',
    component: Grafik,
    beforeEnter: (to, from, next) => {
      if (!Store.getters['auth/authenticated']) {
        return next({
          path: '/login'
        })
      }
      next()
    }
  },
  {
    path: '/laporan',
    name: 'Laporan',
    component: Laporan,
    beforeEnter: (to, from, next) => {
      if (!Store.getters['auth/authenticated']) {
        return next({
          path: '/login'
        })
      }
      next()
    }
  },
  {
    path: '/pemesanan',
    name: 'pemesanan',
    component: Pemesanan,
    beforeEnter: (to, from, next) => {
      if (!Store.getters['auth/authenticated']) {
        return next({
          path: '/login'
        })
      }
      next()
    }
  },
  {
    path: '/penjualan/:id',
    name: 'penjualan',
    component: Penjualan,
    beforeEnter: (to, from, next) => {
      if (!Store.getters['auth/authenticated']) {
        return next({
          path: '/login'
        })
      }
      next()
    }
  },
  {
    path: '/login',
    name: 'Login',
    component: Login,
    beforeEnter: (to, from, next) => {
      if (Store.getters['auth/authenticated']) {
        return next({
          path: '/'
        })
      }
      next()
    }
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.VUE_APP_BASE,
  routes
})

export default router