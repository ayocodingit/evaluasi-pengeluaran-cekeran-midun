import Vue from 'vue'
import Vuex from 'vuex'
import auth from './auth'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    name: process.env.VUE_APP_NAME,
    color: 'red darken-4',
    name_full: process.env.VUE_APP_NAME_FULL
  },
  mutations: {
    //
  },
  actions: {
    //
  },
  modules: {
    auth
  }
})