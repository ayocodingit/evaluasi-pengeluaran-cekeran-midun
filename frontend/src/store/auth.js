import axios from 'axios'

export default {
  namespaced: true,
  state: {
    token: null,
    user: null,
  },
  getters: {
    authenticated(state) {
      return state.token && state.user
    },
    user(state) {
      return state.user
    },
  },
  mutations: {
    SET_TOKEN(state, token) {
      state.token = token
    },
    SET_USER(state, user) {
      state.user = user
    }
  },
  actions: {
    async login({
      dispatch
    }, credentials) {
      const response = await axios.post('/login', credentials);
      return dispatch('attempt', response.data.result.token)
    },
    async attempt({
      commit,
      state
    }, token) {
      if (token) {
        commit('SET_TOKEN', token);
      }

      if (!state.token) {
        return
      }

      try {
        const response = await axios.post('/me')
        commit('SET_USER', response.data.result)
      } catch (e) {
        commit('SET_USER', null)
        commit('SET_TOKEN', null)
      }
    },
    logout({
      commit
    }){
      return axios.post('/logout').then(() => {
        commit('SET_USER', null)
        commit('SET_TOKEN', null)
      })
    }
  }
}