import store from "@/store"
import axios from "axios"
import Cookies from 'js-cookie'

store.subscribe((mutations) => {
  switch (mutations.type) {
    case 'auth/SET_TOKEN':
      if (mutations.payload) {
        axios.defaults.headers.common['Authorization'] = 'bearer ' + mutations.payload
        Cookies.set('token', mutations.payload)
      } else {
        Cookies.remove('token')
      }
      break;
  }
})