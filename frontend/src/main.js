import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';
import i18n from './plugins/i18n';
import './plugins/Array';
import Axios from 'axios'
import Cookies from 'js-cookie'
import VueMeta from 'vue-meta'
import Multiselect from 'vue-multiselect'
import '@/store/subscribe'
import 'sweetalert2/dist/sweetalert2.min.css';
import VueSweetalert2 from 'vue-sweetalert2';
import { BootstrapVue, BootstrapVueIcons } from 'bootstrap-vue'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import Loading from 'vue-loading-overlay';
import 'vue-loading-overlay/dist/vue-loading.css';
import HighchartsVue from 'highcharts-vue'
import VueMonthlyPicker from 'vue-monthly-picker'
import './assets/css/main.css'
import './registerServiceWorker'
Vue.use(VueMeta)
Vue.use(BootstrapVue)
Vue.use(BootstrapVueIcons)
Vue.component('multiselect', Multiselect)
Vue.component('vue-monthly-picker', VueMonthlyPicker)
Vue.component('loading', Loading)
Vue.use(HighchartsVue)

Axios.defaults.baseURL = process.env.VUE_APP_API_URL
Vue.prototype.$axios = Axios

Vue.config.productionTip = false;
Vue.use(VueSweetalert2);

store.dispatch('auth/attempt', Cookies.get('token')).then(() => {
  new Vue({
    router,
    store,
    vuetify,
    i18n,
    render: h => h(App)
  }).$mount('#app')
})

